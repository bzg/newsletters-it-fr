# Liste des newsletters IT francophones

Une liste de newsletters IT en français. Cette liste est participative. N'hésitez pas à proposer une Merge Request pour ajouter votre newsletter ! Si vous ne savez pas comment faire, contactez-moi directement (contact tout en bas).

### Communauté Logiciel Libre/Open Source

* Lettre d'information Framasoft https://contact.framasoft.org/fr/newsletter/
* Le Courrier du hacker https://lecourrierduhacker.com/
* La gazette #bluehats https://github.com/DISIC/gazette-bluehats

### Emploi

* Newsletter LinuxJobs.fr http://eepurl.com/b7kQjL

### Web

* La Lettre de la qualité Web https://www.opquast.com/lettre-de-qualite-web-newsletter-opquast/

## Licence
[![CC BY-SA 2.0 FR](https://i.creativecommons.org/l/by-sa/2.0/88x31.png)](https://creativecommons.org/licenses/by-sa/2.0/fr/)

## Éditeur
Carl Chenet - [@carl_chenet](https://twitter.com/carl_chenet) or <carl.chene@mytux.fr>
